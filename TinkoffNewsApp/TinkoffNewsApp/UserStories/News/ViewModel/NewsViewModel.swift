import Domain
import NetworkPlatform

final class NewsViewModel: NewsViewModelProtocol {
	
	private let navigator: NewsNavigator
	private let dataSource: DataSource
	
	let noDataTrigger: Dynamic<Bool>
	let news: Dynamic<[NewsItem]>
	let errorMessage: Dynamic<String?>
	let rowToReload: Dynamic<(indexPath: IndexPath, item: NewsItem)?>
	
	init(navigator: NewsNavigator,
		 dataSource: DataSource) {
		
		self.navigator = navigator
		self.dataSource = dataSource
		errorMessage = Dynamic(nil)
		news = Dynamic([])
		rowToReload = Dynamic(nil)
		noDataTrigger = Dynamic(false)
	}
	
	func fetchNews(pageOffset: Int, pageSize: Int) {
		let success: ([NewsItem]) -> Void = { [weak self] news in
			
			self?.noDataTrigger.value = news.isEmpty
			pageOffset == 0 ? // Reload ???
				self?.news.value = news :
				self?.news.value.append(contentsOf: news)
		}
		
		dataSource.fetchNews(pageOffset: pageOffset,
							 pageSize: pageSize,
							 success: success,
							 failure: { [weak self] errorMsg in self?.errorMessage.value = errorMsg })
	}
	
	func showNewsDetails(from newsItem: NewsItem) {
		navigator.toNewsDetails(newsItem, outputHandler: self)
	}
}

extension NewsViewModel: NewsDetailsModuleOutput {
	
	func userDidReadNewsDetails(in newsItem: NewsItem) {
		
		guard let index = news.value.index(of: newsItem) else { return }
		news.value[index] = newsItem
		let indexPath = IndexPath(row: index, section: 0)
		rowToReload.value = (indexPath: indexPath, item: newsItem)
	}
}
