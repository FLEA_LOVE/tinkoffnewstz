import Domain

protocol NewsViewModelProtocol: BaseViewModelProtocol {
	
	var noDataTrigger: Dynamic<Bool> { get }
	var news: Dynamic<[NewsItem]> { get }
	var rowToReload: Dynamic<(indexPath: IndexPath, item: NewsItem)?> { get }
	
	func fetchNews(pageOffset: Int, pageSize: Int)
	func showNewsDetails(from newsItem: NewsItem)
}
