import UIKit
import Domain
import NetworkPlatform

class NewsViewController: BaseViewController {

	// MARK: - Outlets
	
	@IBOutlet weak var noInternetView: UIView!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var noInternetConnectionTopConstaint: NSLayoutConstraint!
	
	// MARK: - Fields
	
	var refreshControl = UIRefreshControl()
	
	var shouldShowLoadingCell = true
	private var currentPageOffSet = 0
	private var pageSize: Int {
		return Constants.NewsModule.pageSize
	}
	
	private var tdm = NewsTDM<NewsViewController>()
	
	var viewModel: NewsViewModelProtocol! {
		didSet {
			viewModel.errorMessage.bind { [weak self] error in
				guard let errorMessage = error else { return }
				self?.presentError(message: errorMessage)
			}
			
			viewModel.noDataTrigger.bind { [weak self] noData in
				guard noData else {
					self?.shouldShowLoadingCell = true
					return
				}
				self?.shouldShowLoadingCell = false
				DispatchQueue.main.async {
					self?.tableView.reloadData()
				}
			}
			
			viewModel.news.bind { [weak self] news in
				guard let `self` = self else { return }
				self.tdm.updateItems(news)
			}
			
			viewModel.rowToReload.bind { [weak self] pair in
				guard let `self` = self, let pair = pair else { return }
				
				let indexPath = pair.indexPath
				let item = pair.item
				
				self.tdm.items[indexPath.row] = item
				DispatchQueue.main.async {
					self.tableView.reloadRows(at: [indexPath], with: .automatic)
				}
			}
		}
	}
	
	// MARK: - View life cycle
	
    override func viewDidLoad() {
        super.viewDidLoad()
		setupUI()
		loadNews()
		
    }
	
	// MARK: - Setup
	
	private func setupUI() {
		setupNavBar()
		setupTableView()
		setupRefreshControl()
		setupReachability()
	}
	
	private func setupNavBar() {
		title = "News"
	}
	
	private func setupTableView() {
		tdm.delegate = self
		tableView.register(nibModels: [NewsCellModel.self])
		tableView.tableFooterView = UIView()
		tableView.separatorInset = .zero
		tableView.delegate = tdm.delegate(forTableView: tableView)
		tableView.dataSource = tdm.dataSource(forTableView: tableView)
	}
	
	private func setupRefreshControl() {
		refreshControl.backgroundColor = .white
		refreshControl.tintColor = .orange
		refreshControl.addTarget(self,
								 action: #selector(handleRefreshControl),
								 for: UIControlEvents.valueChanged)
		tableView.refreshControl = refreshControl
	}
	
	private func setupReachability() {
		reachability.whenReachable = { [weak self] _ in
			guard let `self` = self else { return }
			self.changeNoInternetConnectionTopConstaint(to: 0)
			
			DispatchQueue.main.async {
				if !self.shouldShowLoadingCell {
					self.shouldShowLoadingCell = true
					self.tableView.reloadData()
				}
			}
		}
		reachability.whenUnreachable = { [weak self] _ in
			self?.changeNoInternetConnectionTopConstaint(to: 60)
		}
	}
	
	private func changeNoInternetConnectionTopConstaint(to newConstant: CGFloat) {
		UIView.animate(withDuration: 0.3) {
			self.noInternetConnectionTopConstaint.constant = newConstant
			self.view.layoutIfNeeded()
		}
	}
	// MARK: - Actions
	
	@objc private func handleRefreshControl() {
		currentPageOffSet = 0
		loadNews()
	}
	
	// MARK: - Private
	
	private func loadNews() {
		viewModel.fetchNews(pageOffset: currentPageOffSet,
							pageSize: pageSize)
	}
	
}

// MARK: - NewsTDMDelegate

extension NewsViewController: NewsTDMDelegate {
	
	func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
		guard shouldShowLoadingCell else { return false }
		return indexPath.row == tdm.items.count
	}
	
	func fetchNextPage() {
		currentPageOffSet += pageSize
		loadNews()
	}
	
	func itemsUpdated() {
		DispatchQueue.main.async {
			self.tableView.refreshControl?.endRefreshing()
			self.tableView.reloadData()
		}
	}
	
	func itemSelected(_ item: NewsItem) {
		viewModel.showNewsDetails(from: item)
	}
	
}
