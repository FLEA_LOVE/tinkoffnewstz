import UIKit

class NewsCell: UITableViewCell {
	
	// MARK: - Outlets
	
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var viewedCounterLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	
}
