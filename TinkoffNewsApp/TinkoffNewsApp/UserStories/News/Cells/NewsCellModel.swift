import Foundation
import Domain

struct NewsCellModel {
	/// Заголовок новости
	let title: String
	/// Сколько раз просмотрена новость
	let viewedTimeCount: Int
	/// Дата новости
	let date: Date
	
	init(newsItem: NewsItem) {
		title = newsItem.title
		viewedTimeCount = newsItem.viewedTimeCount
		date = newsItem.createdTime
	}
}

extension NewsCellModel: CellViewModel {
	func setup(cell: NewsCell) {
		cell.titleLabel.text = title
		cell.dateLabel.text = date.HH_mm_dd_MMMM
		cell.viewedCounterLabel.text = String(viewedTimeCount)
	}
}
