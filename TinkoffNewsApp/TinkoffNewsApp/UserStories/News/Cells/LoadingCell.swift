import UIKit

class LoadingCell: UITableViewCell {
	
	var activityIndicator: UIActivityIndicatorView!
	
	override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupSubviews()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupSubviews() {
		let indicator = UIActivityIndicatorView()
		indicator.color = UIColor.orange
		indicator.translatesAutoresizingMaskIntoConstraints = false
		indicator.activityIndicatorViewStyle = .gray
		indicator.hidesWhenStopped = true
		
		contentView.addSubview(indicator)
		
		NSLayoutConstraint.activate([
			indicator.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
			indicator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 10),
			indicator.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
			])
		
		indicator.startAnimating()
	}
}
