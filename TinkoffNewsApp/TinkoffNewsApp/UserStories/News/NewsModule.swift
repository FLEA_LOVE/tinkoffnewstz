import UIKit
import NetworkPlatform

class NewsModule {
	private var viewController: NewsViewController?
	
	private let navigator: NewsNavigator
	private let dataSource: DataSource
	
	init(navigator: NewsNavigator,
		 dataSource: DataSource) {
		self.navigator = navigator
		self.dataSource = dataSource
	}
	
	var view: NewsViewController {
		guard let view = viewController else {
			viewController = NewsViewController.instantiateFromXib()
			configureModule(viewController!)
			return viewController!
		}
		return view
	}
	
	private func configureModule(_ view: NewsViewController) {
		view.viewModel = NewsViewModel(navigator: navigator,
									   dataSource: dataSource)
	}
}
