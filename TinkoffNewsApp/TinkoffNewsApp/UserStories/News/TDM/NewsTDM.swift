import UIKit
import Domain

protocol NewsTDMDelegate: TDMDelegate {
	var shouldShowLoadingCell: Bool { get }
	
	func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool
	func fetchNextPage()
}

class NewsTDM<Delegate: NewsTDMDelegate>: NSObject,
	TDM,
	UITableViewDataSource,
	UITableViewDelegate
where Delegate.ItemType == NewsItem {
	
	weak var delegate: Delegate?
	
	var items: [Delegate.ItemType] = []

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let isLoadingIndexPath = delegate?.isLoadingIndexPath(indexPath) ?? false
		if isLoadingIndexPath {
			return LoadingCell(style: .default, reuseIdentifier: "loading")
		}
		
		let item = items[indexPath.row]
		let model = NewsCellModel(newsItem: item)
		let cell = tableView.dequeueReusableCell(withModel: model, for: indexPath)
		return cell
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let count = items.count
		let shouldShowLoadingCell = (delegate?.shouldShowLoadingCell ?? false) && !items.isEmpty
		return shouldShowLoadingCell ? count + 1 : count // for showing loading cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		let item = items[indexPath.row]
		delegate?.itemSelected(item)
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		guard let strongDelegate = delegate, strongDelegate.isLoadingIndexPath(indexPath) else { return }
		strongDelegate.fetchNextPage()
	}
}
