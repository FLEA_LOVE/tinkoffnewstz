import UIKit
import NetworkPlatform
import Domain

protocol NewsNavigator {
	func toNews()
	func toNewsDetails(_ newsItem: NewsItem, outputHandler: NewsDetailsModuleOutput)
}

class DefaultNewsNavigator: NewsNavigator {
	
	private let navigationController: UINavigationController
	private let dataSource: DataSource
	
	init(navigationController: UINavigationController,
		 dataSource: DataSource) {
		self.navigationController = navigationController
		self.dataSource = dataSource
	}
	
	func toNews() {
		let module = NewsModule(navigator: self,
								dataSource: dataSource)
		let view = module.view
		navigationController.pushViewController(view, animated: true)
	}
	
	func toNewsDetails(_ newsItem: NewsItem,
					   outputHandler: NewsDetailsModuleOutput) {
		
		let module = NewsDetailsModule(dataSource: dataSource,
									   newsItem: newsItem,
									   outputHandler: outputHandler)
		let view = module.view
		navigationController.pushViewController(view, animated: true)
	}
}
