import UIKit
import Domain

class NewsDetailsViewController: BaseViewController {
	
	// MARK: - Outlets
	@IBOutlet weak var webView: UIWebView!
	@IBOutlet weak var activityInficator: UIActivityIndicatorView!
	@IBOutlet weak var placeholder: UIView!
	@IBOutlet weak var noInternetImage: UIImageView!
	
	// MARK: - Fields
	var viewModel: NewsDetailsViewModelProtocol! {
		didSet {
			
			viewModel.errorMessage.bind { [weak self] error in
				guard error != nil else { return }
				self?.showPlaceHolder()
			}
			
			viewModel.newsDetails.bind { [weak self] newsDetails in
				guard let newsDetails = newsDetails  else { return }
				self?.display(text: newsDetails.text)
			}
		}
	}
	
	// MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
		fetchNewsDetails()
    }
	
	// MARK: - Private
	private func display(text: String) {
		DispatchQueue.main.async {
			self.webView.loadHTMLString(text, baseURL: nil)
		}
	}
	
	private func fetchNewsDetails() {
		viewModel.fetchNewsDetails()
	}
	
	private func showPlaceHolder() {
		DispatchQueue.main.async {
			self.placeholder.isHidden = false
			UIView.animate(withDuration: 0.3) {
				self.placeholder.alpha = 1.0
				self.noInternetImage.alpha = 1.0
				self.placeholder.backgroundColor = UIColor(r: 255, g: 165, b: 52, alpha: 1.0)
			}
		}
	}
}

// MARK: - UIWebViewDelegate
extension NewsDetailsViewController: UIWebViewDelegate {
	
	func webViewDidFinishLoad(_ webView: UIWebView) {
		activityInficator.stopAnimating()
		viewModel.userDidReadNewsDetails()
	}
	
	func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
		presentError(message: error.localizedDescription)
	}
}
