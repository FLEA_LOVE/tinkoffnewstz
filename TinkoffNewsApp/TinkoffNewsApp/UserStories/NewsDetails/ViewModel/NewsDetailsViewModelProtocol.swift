import Domain

protocol NewsDetailsViewModelProtocol: BaseViewModelProtocol {
	var newsDetails: Dynamic<NewsDetailsItem?> { get }
	
	func fetchNewsDetails()
	func userDidReadNewsDetails()
}
