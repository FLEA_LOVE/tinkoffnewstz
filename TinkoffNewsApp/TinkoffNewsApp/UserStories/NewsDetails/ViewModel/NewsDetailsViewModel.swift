import Domain
import CoreDataPlatform

final class NewsDetailsViewModel: NewsDetailsViewModelProtocol {
	
	private let dataSource: DataSource
	private let news: NewsItem
	
	private weak var outputHandler: NewsDetailsModuleOutput?
	
	let errorMessage: Dynamic<String?>
	let newsDetails: Dynamic<NewsDetailsItem?>
	
	init(dataSource: DataSource,
		 news: NewsItem,
		 outputHandler: NewsDetailsModuleOutput?) {
		self.dataSource = dataSource
		self.news = news
		self.outputHandler = outputHandler
		errorMessage = Dynamic(nil)
		newsDetails = Dynamic(nil)
	}
	
	func fetchNewsDetails() {
		dataSource.fetchNewsDetails(from: news,
									success: { [weak self] details in self?.newsDetails.value = details },
									failure: { [weak self] errorDesc in self?.errorMessage.value = errorDesc })
	}
	
	func userDidReadNewsDetails() {
		let viewedTime = news.viewedTimeCount + 1
		let readedNews = NewsItem(id: news.id,
								  title: news.title,
								  createdTime: news.createdTime,
								  slug: news.slug,
								  viewedTimeCount: viewedTime)
		
		dataSource.update(newsItem: readedNews,
						  success: { },
						  failure: { [weak self] errorMsg in self?.errorMessage.value = errorMsg })

		outputHandler?.userDidReadNewsDetails(in: readedNews)
	}
}
