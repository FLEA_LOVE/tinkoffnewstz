import Domain

protocol NewsDetailsModuleOutput: class {
	func userDidReadNewsDetails(in newsItem: NewsItem)
}
