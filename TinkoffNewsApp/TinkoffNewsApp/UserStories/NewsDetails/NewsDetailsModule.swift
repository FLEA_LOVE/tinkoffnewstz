import UIKit
import Domain
import NetworkPlatform

class NewsDetailsModule {
	private var viewController: NewsDetailsViewController?
	
	private let newsItem: NewsItem
	private let dataSource: DataSource
	private weak var outputHandler: NewsDetailsModuleOutput?
	
	init(dataSource: DataSource,
		 newsItem: NewsItem,
		 outputHandler: NewsDetailsModuleOutput) {
		self.dataSource = dataSource
		self.newsItem = newsItem
		self.outputHandler = outputHandler
	}
	
	var view: NewsDetailsViewController {
		guard let view = viewController else {
			viewController = NewsDetailsViewController.instantiateFromXib()
			configureModule(viewController!)
			return viewController!
		}
		return view
	}
	
	private func configureModule(_ view: NewsDetailsViewController) {
		
		view.viewModel = NewsDetailsViewModel(dataSource: dataSource,
											  news: newsItem,
											  outputHandler: outputHandler)
	}
}
