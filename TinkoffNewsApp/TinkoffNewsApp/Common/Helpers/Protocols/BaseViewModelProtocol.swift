
protocol BaseViewModelProtocol {
	var errorMessage: Dynamic<String?> { get }
}
