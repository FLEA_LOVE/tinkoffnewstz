import UIKit

protocol XibDesignable : class {}

extension XibDesignable where Self: UIViewController {
	
	static func instantiateFromXib() -> Self {
		
		let dynamicMetatype = Self.self
		let bundle = Bundle(for: dynamicMetatype)
		let name = "\(dynamicMetatype)"
		
		return Self(nibName: name, bundle: bundle)
	}
}

extension UIViewController : XibDesignable {}
