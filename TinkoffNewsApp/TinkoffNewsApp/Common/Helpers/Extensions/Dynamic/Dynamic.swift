import Foundation

/// Класс обеспечивает уведомление View о изменении состояния ViewModel.
/// UPD: 11.03.2018: Поддерживается множественная подписка.
///
/// - note: Подробнее [Web site](https://www.toptal.com/ios/swift-tutorial-introduction-to-mvvm),
/// раздел 'Making the ViewModel Dynamic'.
///
/// Например:
/// **SomeViewModel.swift**
///
///     class SomeViewModel()
/// Объявление объекта, видимого во View:
///
///     let error: Dynamic<Error> { get }
///
/// Инициализация пустым значением:
///
///     - init() {
///        self.error = Dynamic(nil)
///        ...
///     }
/// Измение значения:
///
///     func errorReceived(error: NSError) {
///		   self.error.value = error
///     }
/// В **SomeViewController.swift** сработает подписка.
class Dynamic<T> {
	typealias Listener = (T) -> ()
	var listeners: [Listener] = []
	var weakPointers = NSPointerArray.weakObjects()
	
	/// Метод добавляет подписку на изменение значения наблюдаемого объекта.
	///
	/// - Parameters:
	///      - listener: Замыкание к событию didSet наблюдаемого объекта.
	///		 - target: Объект который подписывается на событие.
	///
	/// Например:
	/// **SomeViewController.swift**
	///
	///     viewModel: SomeViewModel { didSet {
	///         viweModel.error.bind { [weak self] in error
	///             print(error)
	///         }
	///         ....
	///     }
	///
	/// - note: *Target* хранится в виде слабой ссылки.
	/// Перед привязкой нового объекта происходит очистка списка указателей и слушателей от пустых ссылок.
	func bind(_ listener: Listener?, target: AnyObject) {
		// MARK: Cleanup
		let newPointers = NSPointerArray.weakObjects()
		var newListeners: [Listener] = []
		for index in 0 ... weakPointers.count {
			guard let obj = weakPointers.object(at: index) else {
				continue
			}
			let listener = listeners[index]
			newPointers.addObject(obj)
			newListeners.append(listener)
		}
		// MARK: shouldAddNewListener
		var shouldAddNewListener = true
		for index in 0 ... weakPointers.count {
			guard let obj = weakPointers.object(at: index), obj === target else {
				continue
			}
			shouldAddNewListener = false
			break
		}
		// MARK: Modificate arrays if needed.
		if shouldAddNewListener, let newListener = listener {
			newPointers.addObject(target)
			newListeners.append(newListener)
		}
		self.weakPointers = newPointers
		self.listeners = newListeners
	}
	
	/// Метод добавляет подписку на изменение значения наблюдаемого объекта.
	///
	/// - Parameters:
	///      - listener: Замыкание к событию didSet наблюдаемого объекта.
	///
	/// Например:
	/// **SomeViewController.swift**
	///
	///     viewModel: SomeViewModel { didSet {
	///         viweModel.error.bind { [weak self] in error
	///             print(error)
	///         }
	///         ....
	///     }
	///
	func bind(_ listener: Listener?) {
		guard listeners.isEmpty else {
			fatalError("Array of 'listeners' is not empty. Please use 'bind(_ listener: Listener?, target: AnyObject)' in this case.")
		}
		guard let newListener = listener else {
			self.listeners = []
			return
		}
		self.listeners = [newListener]
	}
	
	/// Наблюдаемый объект.
	///
	/// При измении происходит уведомление всех подписчиков.
	var value: T {
		didSet {
			listeners.forEach { $0(value) }
		}
	}
	
	init(_ v: T) {
		value = v
	}
	
	/// Принудительно уведомляет подписчиков о текущем состоянии объекта.
	func fire() {
		listeners.forEach { $0(value) }
	}
	/// Обновляет хранимое значение без срабатывания подписок.
	func updateSilently(_ value: T) {
		let listeners = self.listeners
		self.listeners = []
		self.value = value
		self.listeners = listeners
	}
}
