import Foundation

extension Date {
	var HH_mm_dd_MMMM: String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "HH:mm d MMMM"
		dateFormatter.locale = Locale(identifier: "ru_RU")
		return dateFormatter.string(from: self)
	}
}
