import UIKit

extension UITableView {
	func dequeueReusableCell (withModel model: CellAnyViewModel, for indexPath: IndexPath) -> UITableViewCell {
		let identifier = String(describing: type(of: model).cellAnyType)
		let cell = self.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
		model.setupAnyCell(cell: cell)
		return cell
	}
	
	func register(nibModels: [CellAnyViewModel.Type]) {
		for model in nibModels {
			let identifier = String(describing: model.cellAnyType)
			let nib = UINib(nibName: identifier, bundle: nil)
			self.register(nib, forCellReuseIdentifier: identifier)
		}
	}
}
