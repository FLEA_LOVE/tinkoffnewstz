import NetworkPlatform
import CoreDataPlatform
import Domain

class DataSource {
	let networkService: NetworkService
	let storage: AbstactRepository
	
	init(networkService: NetworkManager,
		 storage: AbstactRepository) {
		self.networkService = networkService
		self.storage = storage
	}
	
	func fetchNews(pageOffset: Int,
				   pageSize: Int,
				   success: @escaping ([NewsItem]) -> Void,
				   failure: @escaping (String) -> Void ) {
		
		networkService.getNews(pageOffset: pageOffset,
							   pageSize: pageSize) { [weak self] news, error in
			
			guard let `self` = self else { return }
								
			guard error == nil, let newsItems = news, !newsItems.isEmpty else {
				
					let repoSuccess: ([NewsItem]) -> Void = { newsFromRepo in
				
						let lastIndex = max(0, newsFromRepo.count - 1)
						let minIndex = min(pageOffset, lastIndex)
						let maxIndex = min(pageOffset + pageSize, lastIndex)
						
						let partOfNews = Array(newsFromRepo[minIndex ..< maxIndex])
						success(partOfNews)
					}
					
					let sortDesc = NSSortDescriptor(key: "createdTime", ascending: false)
					self.storage.fetch(with: nil,
									   sortDescriptors: [sortDesc],
									   success: repoSuccess,
									   failure: { nsError in failure(nsError.description) })
					return
			}
			// Сохрани в бд
			self.storage.save(entities: newsItems,
							  onSuccess: { success($0) },
							  onFailure: { failure($0.description) })
		}
	}
	
	func fetchNewsDetails(from newsItem: NewsItem,
						  success: @escaping (NewsDetailsItem) -> Void,
						  failure: @escaping (String) -> Void) {
		
		networkService.getNewsDetails(from: newsItem) { [weak self] newsDetails, error in
			guard let `self` = self else { return }
			
			guard error == nil, let newsDetailsItem = newsDetails else {
				
				// Доставай из БД
				let predicate = NSPredicate(format: "newsId = %@", newsItem.id as CVarArg)
				self.storage.fetch(with: predicate,
								   sortDescriptors: nil,
								   success: { success($0) },
								   failure: { nsError in failure(nsError.description) })
				return
			}
			
			self.storage.save(entity: newsDetailsItem,
							  onSuccess: { _ in success(newsDetailsItem) },
							  onFailure:  { nsError in failure(nsError.description) })
		}
	}
	
	func update<T: CoreDataRepresentable> (newsItem: T,
										   success: @escaping () -> Void,
										   failure: @escaping (String) -> Void ) {
		
		storage.update(entity: newsItem,
					   onSuccess: success,
					   onFailure: failure)
	}
	
	
	
}
