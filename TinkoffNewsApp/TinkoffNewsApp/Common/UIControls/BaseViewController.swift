import UIKit

class BaseViewController: UIViewController {
	
	// MARK: - Fields
	var reachability: Reachability!
	
	// MARK: - Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		setupReachability()
	}
	
	deinit {
		reachability.stopNotifier()
	}
	
	// MARK: - Setup
	private func setupReachability() {
		reachability = Reachability()
		
		do {
			try reachability.startNotifier()
		} catch {
			presentError(message: "Unable to start notifier", okHandler: { [weak self] in
				self?.setupReachability()
			})
		}
	}
	
	// MARK: - Methods
	
	func presentError(message: String?, okHandler: (() -> Void)? = nil) {
		let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
			okHandler?()
		})
		present(alert, animated: true, completion: nil)
	}
	
	func presentNoInternetConnection() {
		let view = NoInternetConnectionView.instantiateFromXib()
		present(view, animated: true)
	}
}
