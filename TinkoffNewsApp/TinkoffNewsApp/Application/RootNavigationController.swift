import UIKit

final class RootNavigationController: UINavigationController {

	override func viewDidLoad() {
		super.viewDidLoad()
		configureStyle()
	}
	
	private func configureStyle() {
		navigationBar.barTintColor = UIColor.orange
		navigationBar.tintColor = UIColor.white
		navigationBar.shadowImage = UIImage() // delete 1px borders
	}	
}
