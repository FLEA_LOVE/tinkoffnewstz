import Domain
import NetworkPlatform
import CoreDataPlatform

final class Application {
	
	static let shared = Application()
	
	private let dataSource: DataSource
	
	private init() {
		dataSource = DataSource(networkService: NetworkManager(),
								storage: Repository())
	}
	
	func configureMainInterface(in window: UIWindow) {
		let rootNavigation = RootNavigationController()
		let newsNavigator = DefaultNewsNavigator(navigationController: rootNavigation,
												 dataSource: dataSource)
		
		window.rootViewController = rootNavigation
		window.makeKeyAndVisible()
		newsNavigator.toNews()
	}
}
