//
//  CoreDataPlatform.h
//  CoreDataPlatform
//
//  Created by 65apps on 03/07/2018.
//  Copyright © 2018 TestTinkoff. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreDataPlatform.
FOUNDATION_EXPORT double CoreDataPlatformVersionNumber;

//! Project version string for CoreDataPlatform.
FOUNDATION_EXPORT const unsigned char CoreDataPlatformVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreDataPlatform/PublicHeader.h>


