import Foundation
import CoreData


extension CDNewsItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDNewsItem> {
        return NSFetchRequest<CDNewsItem>(entityName: "CDNewsItem")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var title: String?
    @NSManaged public var slug: String?
    @NSManaged public var createdTime: NSDate?
    @NSManaged public var viewedTimeCount: Int32

}
