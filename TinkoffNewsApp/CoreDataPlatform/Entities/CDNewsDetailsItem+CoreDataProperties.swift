import Foundation
import CoreData


extension CDNewsDetailsItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDNewsDetailsItem> {
        return NSFetchRequest<CDNewsDetailsItem>(entityName: "CDNewsDetailsItem")
    }

    @NSManaged public var newsId: UUID?
    @NSManaged public var text: String?

}
