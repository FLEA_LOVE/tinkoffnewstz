import Foundation
import CoreData

/**
Реализуя этот протокол, сущность БД, сообщает, что может быть представлена как тип Domain

- parameters:
- DomainType: Тип ассоцируемой сущности Domain, которая должна быть представляема в БД
- asDomain(): Преобразует сущность в сущность Domain
- update(from entity:): Обновляет сущность БД из сущности Domain
- create(from entity, in context): Создает сущность БД на основании сущности Domain в контесте
*/
public protocol DomainConvertibleType {
	associatedtype DomainType: CoreDataRepresentable
	
	func asDomain() -> DomainType
	
	func update(from entity: DomainType)
	static func create(from entity: DomainType,
					   in context: NSManagedObjectContext) -> DomainType.CoreDataType
}
