import CoreData

/**
Реализуя этот протокол, сущность сообщает, что она представлена в Базе Данных.

- parameters:
- СoreDataType: Какой сущностью она представлена в CoreData.
- entityName: Строковое назнавание сущности БД
- idField: Строковое поле ключа
- id: Значение ключа
- createAssosiatedCDItem: Создает ассоцированную сущность бд в контексте
*/
public protocol CoreDataRepresentable {
	associatedtype CoreDataType: NSManagedObject, DomainConvertibleType
	
	static var entityName: String { get }
	
	var idField: String { get }
	var id: UUID { get }
	
	func createAssosiatedCDItem(in context: NSManagedObjectContext) -> CoreDataType
}

extension CoreDataRepresentable {
	
	public static var entityName: String {
		return CoreDataType.description()
	}
}
