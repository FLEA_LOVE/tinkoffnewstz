import Domain
import CoreData

extension NewsItem: CoreDataRepresentable {
	public typealias CoreDataType = CDNewsItem
	
	public var idField: String {
		return "id"
	}
	
	public func createAssosiatedCDItem(in context: NSManagedObjectContext) -> CoreDataType {
		return CoreDataType.create(from: self, in: context)
	}
}

extension CDNewsItem: DomainConvertibleType {
	
	public static func create(from entity: NewsItem,
					   in context: NSManagedObjectContext) -> CDNewsItem {
		
		let cdNewsItem = CDNewsItem(context: context)
		cdNewsItem.update(from: entity)
		return cdNewsItem
	}
	
	public func asDomain() -> NewsItem {
		return NewsItem(id: id!,
						title: title!,
						createdTime: createdTime! as Date,
						slug: slug!,
						viewedTimeCount: Int(viewedTimeCount))
	}
	
	public func update(from entity: NewsItem) {
		id = entity.id
		title = entity.title
		createdTime = entity.createdTime as NSDate
		slug = entity.slug
		
		update(viewedTimeCount: entity.viewedTimeCount)
	}
	
	fileprivate func update(viewedTimeCount: Int) {
		guard viewedTimeCount != 0 else { return }
		self.viewedTimeCount = Int32(viewedTimeCount)
	}
}
