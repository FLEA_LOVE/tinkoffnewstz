import Domain
import CoreData

extension CDNewsDetailsItem: DomainConvertibleType {

	public func asDomain() -> NewsDetailsItem {
		return NewsDetailsItem(newsId: newsId!, text: text!)
	}
	
	public func update(from entity: NewsDetailsItem) {
		newsId = entity.newsId
		text = entity.text
	}
	
	public static func create(from entity: NewsDetailsItem,
							  in context: NSManagedObjectContext) -> CDNewsDetailsItem {
		
		let cdNewsDetailsItem = CDNewsDetailsItem(context: context)
		cdNewsDetailsItem.update(from: entity)
		return cdNewsDetailsItem
	}

}

extension NewsDetailsItem: CoreDataRepresentable {
	
	public typealias CoreDataType = CDNewsDetailsItem
	
	public var idField: String {
		return "newsId"
	}
	
	public var id: UUID {
		return newsId
	}
	
	public func createAssosiatedCDItem(in context: NSManagedObjectContext) -> CoreDataType {
		
		return CoreDataType.create(from: self, in: context)
	}
}
