import CoreData
import Domain

public protocol AbstactRepository {
	
	func fetch<T: CoreDataRepresentable> (with predicate: NSPredicate?,
										  sortDescriptors: [NSSortDescriptor]?,
										  success: @escaping (T) -> Void,
										  failure: @escaping (NSError) -> Void)
	
	func fetch<T: CoreDataRepresentable> (with predicate: NSPredicate?,
										  sortDescriptors: [NSSortDescriptor]?,
										  success: ([T]) -> Void,
										  failure: (NSError) -> Void)
	
	func save<T: CoreDataRepresentable> (entity: T,
										 onSuccess: @escaping ([T]) -> Void,
										 onFailure: @escaping (NSError) -> Void)
	
	func save<T: CoreDataRepresentable> (entities: [T],
										 onSuccess: @escaping ([T]) -> Void,
										 onFailure: @escaping (NSError) -> Void)
	
	func update<T: CoreDataRepresentable> (entity: T,
										   onSuccess: @escaping () -> Void,
										   onFailure: @escaping (String) -> Void)
}

public class Repository: AbstactRepository {
	
	private let coreDataStack: CoreDataStackProtocol
	
	public init() {
		self.coreDataStack = CoreDataStack()
	}
	
	public func fetch<T: CoreDataRepresentable> (with predicate: NSPredicate?,
												 sortDescriptors: [NSSortDescriptor]?,
												 success: @escaping (T) -> Void,
												 failure: @escaping (NSError) -> Void) {
		
		let onSuccess: ([T]) -> Void = { (items: [T]) in
			
			guard let first = items.first else {
				failure(RepositoryError.itemNotFound as NSError)
				return
			}
			success(first)
		}
		
		fetch(with: predicate,
			  sortDescriptors: sortDescriptors,
			  success: onSuccess,
			  failure: failure)
	}
	
	public func fetch<T: CoreDataRepresentable> (with predicate: NSPredicate?,
												  sortDescriptors: [NSSortDescriptor]?,
												  success: ([T]) -> Void,
												  failure: (NSError) -> Void) {
		
		let request = NSFetchRequest<T.CoreDataType>(entityName: T.entityName)
		request.predicate = predicate
		request.sortDescriptors = sortDescriptors
		
		do {
			let result = try coreDataStack.context.fetch(request)
			let items = result.map { $0.asDomain() as! T} // as! T - с точки зрения кода это и есть T, компилятор этого не понимает
			success(items)
		} catch let error as NSError {
			failure(error)
		}
	}

	public func save<T: CoreDataRepresentable> (entity: T,
												onSuccess: @escaping ([T]) -> Void,
												onFailure: @escaping (NSError) -> Void) {
		save(entities: [entity],
			 onSuccess: { onSuccess($0) },
			 onFailure: onFailure)
	}

	public func save<T: CoreDataRepresentable> (entities: [T],
												onSuccess: @escaping ([T]) -> Void,
												onFailure: @escaping (NSError) -> Void) {
		var savedEntities: [T] = []
		for entity in entities {
			
			guard let storedEntity = tryToFind(entity: entity) else {
				let newEntity = entity.createAssosiatedCDItem(in: coreDataStack.context)
				
				let domainCompliliant = newEntity.asDomain() as! T
				savedEntities.append(domainCompliliant)
				
				continue
			}
			let compiliantEntity = entity as! T.CoreDataType.DomainType
			storedEntity.update(from: compiliantEntity)
			
			let domainCompliliant = storedEntity.asDomain() as! T
			savedEntities.append(domainCompliliant)
		}
		
		do {
			try coreDataStack.saveContext()
			onSuccess(savedEntities)
		} catch let error as NSError {
			onFailure(error)
		}
	}
	
	public func update<T: CoreDataRepresentable> (entity: T,
												  onSuccess: @escaping () -> Void,
												  onFailure: @escaping (String) -> Void) {
		
		guard let storedEntity = tryToFind(entity: entity) else {
			onFailure(RepositoryError.itemNotFound.localizedDescription)
			return
		}
		let compiliantEntity = entity as! T.CoreDataType.DomainType
		storedEntity.update(from: compiliantEntity)
		
		do {
			try coreDataStack.saveContext()
			onSuccess()
		} catch let error as NSError {
			onFailure(error.localizedDescription)
		}
	}

	// MARK: - Private
	private func tryToFind<T: CoreDataRepresentable>(entity: T) -> T.CoreDataType?  {
		
		let request = NSFetchRequest<T.CoreDataType>(entityName: T.entityName)
		request.predicate = NSPredicate(format: "\(entity.idField) = %@", entity.id as CVarArg)
		
		let result = try? coreDataStack.context.fetch(request)
		return result?.first
	}
	
	
}
