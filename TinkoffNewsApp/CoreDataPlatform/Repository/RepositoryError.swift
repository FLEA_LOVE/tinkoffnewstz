public enum RepositoryError: Error {
	case itemNotFound
	
}

extension RepositoryError: LocalizedError {
	public var errorDescription: String? {
		switch self {
		case .itemNotFound:
			return NSLocalizedString("This item not found.", comment: "Error")
		}
	}
}
