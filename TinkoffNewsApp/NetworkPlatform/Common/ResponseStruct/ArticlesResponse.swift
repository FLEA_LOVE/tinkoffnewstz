import Domain

struct ArticlesBaseResponse {
	let response: ArticlesResponse
}

extension ArticlesBaseResponse: Decodable {
	
	private enum ArticlesBaseResponseCodingKeys: String, CodingKey {
		case response
	}
	
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: ArticlesBaseResponseCodingKeys.self)
		
		response = try container.decode(ArticlesResponse.self, forKey: .response)
	}
}

struct ArticlesResponse {
	
	let news: [NewsItem]
	let total: Int
}

extension ArticlesResponse: Decodable {
	
	private enum ArticlesResponseCodingKeys: String, CodingKey {
		case news
		case total
	}
	
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: ArticlesResponseCodingKeys.self)
		
		news = try container.decode([NewsItem].self, forKey: .news)
		total = try container.decode(Int.self, forKey: .total)
	}
}
