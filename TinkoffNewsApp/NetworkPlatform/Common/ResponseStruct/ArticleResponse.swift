import Domain

struct ArticleResponse {
	let response: NewsDetailsItem
}

extension ArticleResponse: Decodable {
	
	private enum ArticleResponseCodingKeys: String, CodingKey {
		case response
	}
	
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: ArticleResponseCodingKeys.self)
		
		response = try container.decode(NewsDetailsItem.self, forKey: .response)
	}
}
