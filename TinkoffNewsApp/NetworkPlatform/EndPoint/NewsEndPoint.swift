import Foundation

enum NetworkEnvironment {
	case qa
	case production
	case staging
}

public enum NewsApi {
	
	case articles(pageSize: Int, pageOffset: Int)
	case article(urlSlug: String)
}

extension NewsApi: EndPointType {
	
	var enviromentBaseURL: String {
		switch NetworkManager.enviroment {
		case .production: return "https://cfg.tinkoff.ru/news/public/api/platform/v1/"
		case .qa: return "https://cfg.tinkoff.ru/news/public/api/platform/v1/"
		case .staging: return "https://cfg.tinkoff.ru/news/public/api/platform/v1/"
		}
	}
	
	var baseURL: URL {
		guard let url = URL(string: enviromentBaseURL) else { fatalError("baseURL could not be configured.") }
		return url
	}
	
	var path: String {
		switch self {
		case .articles:
			return "getArticles"
		case .article:
			return "getArticle"
		}
	}
	
	var httpMethod: HTTPMethod {
		return .get
	}
	
	var task: HTTPTask {
		switch self {
		case .articles(let pageSize, let pageOffset):
			return .requestParameters(bodyParameters: nil,
									  urlParameters: ["pageSize": pageSize,
													  "pageOffset": pageOffset])
		case .article(let urlSlug):
			return .requestParameters(bodyParameters: nil,
									  urlParameters: ["urlSlug": urlSlug])
		}
	}
	
	var headers: HTTPHeaders? {
		return nil
	}
}
