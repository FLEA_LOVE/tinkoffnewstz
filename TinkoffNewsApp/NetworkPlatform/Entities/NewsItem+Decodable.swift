import Domain

extension NewsItem: Decodable {
	
	private enum NewsItemCodingKeys: String, CodingKey {
		case id
		case title
		case createdTime
		case slug
	}
	
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: NewsItemCodingKeys.self)
		
		id = try container.decode(UUID.self, forKey: .id)
		title = try container.decode(String.self, forKey: .title)
		createdTime = try container.decode(Date.self, forKey: .createdTime)
		slug = try container.decode(String.self, forKey: .slug)
		viewedTimeCount = 0
	}
}
