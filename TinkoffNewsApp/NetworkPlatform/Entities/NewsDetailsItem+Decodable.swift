import Domain

extension NewsDetailsItem: Decodable {
	
	private enum NewsDetailsItemCodingKeys: String, CodingKey {
		case newsId = "id"
		case text
	}
	
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: NewsDetailsItemCodingKeys.self)
		
		newsId = try container.decode(UUID.self, forKey: .newsId)
		text = try container.decode(String.self, forKey: .text)
	}
}
