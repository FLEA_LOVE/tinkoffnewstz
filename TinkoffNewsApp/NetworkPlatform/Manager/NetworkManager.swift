import Domain

public protocol NetworkService {
	
	typealias newsItemsCompletion = (_ newsItems: [NewsItem]?, _ error: String?) -> ()
	func getNews(pageOffset: Int,
				 pageSize: Int,
				 completion: @escaping newsItemsCompletion)
	
	typealias newsDetailsCompletion = (_ newsDetails: NewsDetailsItem?, _ error: String?) -> ()
	func getNewsDetails(from news: NewsItem,
						completion: @escaping newsDetailsCompletion)
}

public struct NetworkManager: NetworkService {
	
	static let enviroment: NetworkEnvironment = .production
	
	private let router = Router<NewsApi>()
	
	// MARK: Init
	
	public init() { }
	
	// MARK: - Methods
	public func getNews(pageOffset: Int,
						pageSize: Int,
						completion: @escaping newsItemsCompletion) {
		
		router.request(.articles(pageSize: pageSize, pageOffset: pageOffset)) { data, response, error in
			
			guard error == nil else {
				completion(nil, error?.localizedDescription)
				return
			}
			
			if let response = response as? HTTPURLResponse {
				
				let result = self.handleNetworkResponse(response)
				switch result {
				case .success:
					guard let responseData = data else {
						completion(nil, NetworkResponse.noData.rawValue)
						return
					}
					
					do {
						let jsonDecoder = JSONDecoder()
						jsonDecoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601Full)
						let apiResponse = try jsonDecoder.decode(ArticlesBaseResponse.self, from: responseData)
						completion(apiResponse.response.news , nil)
					} catch {
						completion(nil, NetworkResponse.unableToDecode.rawValue)
					}
				case .failure(let networkFailureError):
					completion(nil, networkFailureError)
				}
			}
		}
	}
	
	public func getNewsDetails(from news: NewsItem,
							   completion: @escaping newsDetailsCompletion) {
		
		router.request(.article(urlSlug: news.slug)) { data, response, error in
			
			guard error == nil else {
				completion(nil, error?.localizedDescription)
				return
			}
			
			if let response = response as? HTTPURLResponse {
				
				let result = self.handleNetworkResponse(response)
				switch result {
				case .success:
					guard let responseData = data else {
						completion(nil, NetworkResponse.noData.rawValue)
						return
					}
					
					do {
						let apiResponse = try JSONDecoder().decode(ArticleResponse.self, from: responseData)
						completion(apiResponse.response, nil)
					} catch {
						completion(nil, NetworkResponse.unableToDecode.rawValue)
					}
				case .failure(let networkFailureError):
					completion(nil, networkFailureError)
				}
			}
		}
	}
}

// MARK: - Response
extension NetworkManager {

	fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String> {
		switch response.statusCode {
		case 200...299: return .success
		case 401...500: return .failure(NetworkResponse.authentificationError.rawValue)
		case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
		case 600: return .failure(NetworkResponse.outdated.rawValue)
		default: return .failure(NetworkResponse.failed.rawValue)
		}
	}
	
	enum NetworkResponse: String {
		case success
		case authentificationError = "You need to be authentificated first."
		case badRequest = "Bad request."
		case outdated = "The url you requested is outdated."
		case failed = "Network request failed."
		case noData = "Response returned with no data to decode."
		case unableToDecode = "We could not decode the response."
	}
	
	enum Result<String> {
		case success
		case failure(String)
	}
}
