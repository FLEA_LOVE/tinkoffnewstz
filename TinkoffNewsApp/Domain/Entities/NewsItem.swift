import Foundation

public struct NewsItem {
	
	public let id: UUID
	public let title: String
	public let createdTime: Date
	public let slug: String
	public let viewedTimeCount: Int
	
	public init(id: UUID,
				title: String,
				createdTime: Date,
				slug: String,
				viewedTimeCount: Int) {
		self.id = id
		self.title = title
		self.createdTime = createdTime
		self.slug = slug
		self.viewedTimeCount = viewedTimeCount
	}
}


extension NewsItem: Equatable {
	
	public static func == (lhs: NewsItem, rhs: NewsItem) -> Bool {
		return lhs.id == rhs.id
	}
}
