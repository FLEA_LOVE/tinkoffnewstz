import Foundation

public struct NewsDetailsItem {
	
	public let newsId: UUID
	public let text: String
	
	public init(newsId: UUID, text: String) {
		self.newsId = newsId
		self.text = text
	}
}

extension NewsDetailsItem: Equatable {
	
	public static func ==(lhs: NewsDetailsItem, rhs: NewsDetailsItem) -> Bool {
		return lhs.newsId == rhs.newsId
	}
}
